require("dotenv").load();

const { Kayn, REGIONS } = require("kayn");

const kayn = Kayn(process.env.RIOT_LOL_API_KEY)({
  region: REGIONS.EUROPE_WEST
});

async function run() {
  const { accountId } = await kayn.Summoner.by.name("jamesonthed");

  console.log(accountId);

  //   const matchList = await kayn.Matchlist.by.accountID(accountId);

  //   console.log(matchList);

  //   for (let { gameId } of matchList.matches) {
  //     const matchDetails = await kayn.Match.get(gameId);

  //     console.log(matchDetails);
  //   }
}

run();
